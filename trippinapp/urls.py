from django.urls import path
from . import views

urlpatterns = [
    path('trippin_post/', views.trippin_post, name='trippin_post'),
    path('post_list/', views.post_list, name='post_list'),
    path('', views.login_user, name='login_user'),
    path('submit', views.submit_login, name='submit_login'),
    path('post_list/delete/<process_id>', views.delete_post),
    path("logout", views.logout_request, name="logout"),
    path("register_human_user", views.register_human_user, name="register_human_user"),
    path("register_hotel_user", views.register_hotel_user, name="register_hotel_user"),
    path("register_restaurant_user", views.register_restaurant_user, name="register_restaurant_user"),
    path("register_attraction_user", views.register_attraction_user, name="register_attraction_user"),
]