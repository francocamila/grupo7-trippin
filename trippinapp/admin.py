from django.contrib import admin
from .models import Trip
from .models import Register_human_user
from .models import Register_hotel_user
from .models import Register_restaurant_user
from .models import Register_attraction_user

admin.site.register(Trip)
admin.site.register(Register_human_user)
admin.site.register(Register_hotel_user)
admin.site.register(Register_restaurant_user)
admin.site.register(Register_attraction_user)

# Register your models here.
