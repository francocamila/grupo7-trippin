from django.db import models
from django.conf import settings
from django.utils import timezone
from django.urls import reverse

# Create your models here.

class Trip(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.created_date

class Register_human_user(models.Model):
    human_username = models.TextField(blank=True, null=True)
    human_adress=models.TextField(blank=True, null=True)
    human_birthday=models.TextField(blank=True, null=True)
    human_email = models.TextField(blank=True, null=True)
    human_password = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.human_username

class Register_hotel_user(models.Model):
    hotel_name = models.TextField()
    hotel_adress = models.TextField()
    hotel_price = models.TextField()
    hotel_email = models.TextField()
    hotel_password = models.TextField()
    hotel_amenities = models.TextField()
    hotel_phone_number = models.TextField()

    def __str__(self):
        return self.hotel_name

class Register_restaurant_user(models.Model):
    restaurant_name = models.TextField()
    restaurant_adress = models.TextField()
    restaurant_cuisines = models.TextField()
    restaurant_email = models.TextField()
    restaurant_password = models.TextField()
    restaurant_special_diets = models.TextField()
    restaurant_phone_number = models.TextField()
    restaurant_business_hours = models.TextField()
    tags = models.TextField(default="howdy")

    def __str__(self):
        return self.restaurant_name

class Register_attraction_user(models.Model):
    attraction_name = models.TextField()
    attraction_adress = models.TextField()
    attraction_paid = models.TextField()
    attraction_email = models.TextField()
    attraction_password = models.TextField()
    attraction_business_hours = models.TextField()
    attraction_phone_number = models.TextField()
    
    def __str__(self):
        return self.attraction_name

#def publish(self):
#    self.published_date = timezone.now()
#    self.save()

