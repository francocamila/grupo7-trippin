import sys
sys.setrecursionlimit(1500)
from django.shortcuts import render, redirect
from .models import Trip
from .models import Register_human_user
from .models import Register_hotel_user
from .models import Register_restaurant_user
from .models import Register_attraction_user
import json

from django.http import HttpResponse
#importando tudo da utils, pois nela está os métodos de handling dos inputs
from .utils import *
from django.template import loader
#crud
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
#timezone
from django.utils import timezone
#por causa do erro de csrf
from django.views.decorators.csrf import csrf_exempt
#forms
from .forms import Post_Form
from .forms import Register_human_user_form
from .forms import Register_hotel_user_form
from .forms import Register_restaurant_user_form
from .forms import Register_attraction_user_form
#Creting user?
from django.contrib.auth.models import User
#write csv
from djqscsv import render_to_csv_response
from djqscsv import write_csv
#para adicionar querysets em um csv
import csv
#para adicionar uma linha em resultados.csv
from csv import writer
#para procurar números em uma string
import re
#para converter string para datetime
from datetime import datetime
#authenticator:
from django.contrib.auth import authenticate, login, logout
#para retornar erro
from django.contrib import messages
#login required:
from django.contrib.auth.decorators import login_required
#logout


#Lógica do site: Faz um request pra model e passa para um template. 

@csrf_exempt 
@login_required(login_url='/')
def trippin_post(request):



    #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
    #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
    form = Post_Form(request.POST)
    if request.method == 'POST' and 'run_script' in request.POST:
        if form.is_valid():
            text = request.POST.get("text", None)
        return redirect('/trippin_post/')
    else:
        form = Post_Form()
    #salvando todos os querysets em um csv
    qs = Trip.objects.all()
    #with open('lista_de_posts.csv', 'wb') as csv_file:
    #    write_csv(qs, csv_file)
    return render(request, 'trippinapp/trippin_post.html')

#@login_required(login_url='/')
def register_human_user(request):

    #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
    #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
    form = Register_human_user_form(request.POST)
    if request.method == 'POST' and 'register' in request.POST:
        if form.is_valid():
            human_username = request.POST.get("human_username", None)
            human_adress= request.POST.get("human_adress", None)
            human_birthday= request.POST.get("human_birthday", None)
            human_email = request.POST.get("human_email", None)
            human_password = request.POST.get("human_password", None)
            new_user = User.objects.create_user(human_username, human_email, human_password)
            new_user.save()
            human_user = Register_human_user(human_username=human_username, human_adress=human_adress, human_birthday=human_birthday, human_email=human_email, human_password=human_password)
            human_user.save()
            #salvando todos os querysets em um csv
            qs = Register_human_user.objects.all()
            with open('human_users_list.csv', 'wb') as csv_file:
                write_csv(qs, csv_file)
        return redirect('/')
    else:
        form = Register_human_user_form()
    return render(request, 'trippinapp/register_human_user.html') 
    
def register_hotel_user(request):

    #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
    #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
    form = Register_hotel_user_form(request.POST)
    if request.method == 'POST' and 'register' in request.POST:
        if form.is_valid():
            hotel_name = request.POST.get("hotel_name", None)
            hotel_adress= request.POST.get("hotel_adress", None)
            #hotel_price= request.POST.get("hotel_price", None)
            hotel_email = request.POST.get("hotel_email", None)
            hotel_password = request.POST.get("hotel_password", None)
            #hotel_amenities = request.POST.get("hotel_amenities", None)
            #hotel_phone_number = request.POST.get("hotel_phone_number", None)
            new_user = User.objects.create_user(hotel_name, hotel_email, hotel_password)
            new_user.save()
            hotel_amenities, hotel_price, hotel_phone_number = get_hotel(hotel_name, hotel_adress)
            #get_hotel(hotel_name, hotel_adress)
            hotel_user = Register_hotel_user(hotel_name=hotel_name, hotel_adress=hotel_adress, hotel_price=hotel_price, hotel_email=hotel_email, hotel_password = hotel_password, hotel_amenities=hotel_amenities, hotel_phone_number = hotel_phone_number)
            hotel_user.save()
            #salvando todos os querysets em um csv
            qs = Register_hotel_user.objects.all()
            with open('human_hotel_list.csv', 'wb') as csv_file:
                write_csv(qs, csv_file)
        return redirect('/')
    else:
        form = Register_hotel_user_form()
    return render(request, 'trippinapp/register_hotel_user.html')    

def register_restaurant_user(request):

    #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
    #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
    form = Register_restaurant_user_form(request.POST)
    if request.method == 'POST' and 'register' in request.POST:
        if form.is_valid():
            restaurant_name = request.POST.get("restaurant_name", None)
            restaurant_adress= request.POST.get("restaurant_adress", None)
            restaurant_cuisines= request.POST.get("restaurant_cuisines", None)
            restaurant_email = request.POST.get("restaurant_email", None)
            restaurant_password = request.POST.get("restaurant_password", None)
            restaurant_special_diets = request.POST.get("restaurant_special_diets", None)
            restaurant_phone_number = request.POST.get("restaurant_phone_number", None)
            restaurant_business_hours = request.POST.get("restaurant_business_hours", None)
            new_user = User.objects.create_user(restaurant_name, restaurant_email, restaurant_password)
            new_user.save()
            citycode = json.loads(restaurant_adress)
            latitude = citycode["lat"]
            longitude = citycode["long"]
            tags = get_restaurant(restaurant_name, latitude, longitude)
            restaurant_user = Register_restaurant_user(restaurant_name=restaurant_name, restaurant_adress=restaurant_adress, restaurant_cuisines=restaurant_cuisines, restaurant_email=restaurant_email, restaurant_password=restaurant_password, restaurant_special_diets= restaurant_special_diets, restaurant_phone_number = restaurant_phone_number, restaurant_business_hours = restaurant_business_hours, tags = tags)
            restaurant_user.save()
            #salvando todos os querysets em um csv
            qs = Register_restaurant_user.objects.all()
            with open('human_restaurant_list.csv', 'wb') as csv_file:
                write_csv(qs, csv_file)
            
        return redirect('/')
    else:
        form = Register_restaurant_user_form()
    return render(request, 'trippinapp/register_restaurant_user.html')    

def register_attraction_user(request):

    #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
    #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
    form = Register_attraction_user_form(request.POST)
    if request.method == 'POST' and 'register' in request.POST:
        if form.is_valid():
            attraction_name = request.POST.get("attraction_name", None)
            attraction_adress= request.POST.get("attraction_adress", None)
            attraction_email = request.POST.get("attraction_email", None)
            attraction_password = request.POST.get("attraction_password", None)
            attraction_phone_number = request.POST.get("attraction_phone_number", None)
            attraction_business_hours = request.POST.get("attraction_business_hours", None)
            new_user = User.objects.create_user(attraction_name, attraction_email, attraction_password)
            new_user.save()
            citycode = json.loads(attraction_adress)
            latitude = citycode["lat"]
            longitude = citycode["long"]
            attraction_paid = get_attraction(attraction_name, latitude, longitude)
            attraction_user = Register_attraction_user(attraction_name=attraction_name, attraction_adress=attraction_adress, attraction_paid=attraction_paid, attraction_email=attraction_email, attraction_password=attraction_password, attraction_phone_number = attraction_phone_number, attraction_business_hours= attraction_business_hours)
            attraction_user.save()
            #salvando todos os querysets em um csv
            qs = Register_attraction_user.objects.all()
            with open('human_attraction_list.csv', 'wb') as csv_file:
                write_csv(qs, csv_file)
        return redirect('/')
    else:
        form = Register_attraction_user_form()
    return render(request, 'trippinapp/register_attraction_user.html')  

@login_required(login_url='/')
#views da página de listagem dos processos:    
def post_list(request):
    posts = Trip.objects.filter().order_by('created_date')
    return render(request, 'trippinapp/post_list.html', {'posts':posts})

#views da página de login:
def login_user(request):
    return render(request, 'trippinapp/login.html', {})

#views da autenticação:
def submit_login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect ('/post_list/')
        else:
            messages.error(request, 'Usuário ou senha inválido.')
    return redirect('/')
    
@login_required(login_url='/')
def delete_post(request, process_id):
    post_id = int(post_id)
    try:
        post_sel = Trip.objects.get(id = post_id)
    except Trip.DoesNotExist:
        return redirect('/post_list/')
    post_sel.delete()
    return redirect('/post_list/')

def logout_request(request):
    logout(request)
    messages.info(request, "Usuário deslogado com sucesso!")
    return redirect("/")