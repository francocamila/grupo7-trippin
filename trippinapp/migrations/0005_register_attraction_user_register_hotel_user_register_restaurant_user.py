# Generated by Django 3.1.1 on 2020-10-02 01:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trippinapp', '0004_auto_20200920_2153'),
    ]

    operations = [
        migrations.CreateModel(
            name='Register_attraction_user',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('attraction_name', models.TextField()),
                ('attraction_adress', models.TextField()),
                ('attraction_paid', models.TextField()),
                ('attraction_email', models.TextField()),
                ('attraction_password', models.TextField()),
                ('attraction_business_hours', models.TextField()),
                ('attraction_phone_number', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Register_hotel_user',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hotel_name', models.TextField()),
                ('hotel_adress', models.TextField()),
                ('hotel_price', models.TextField()),
                ('hotel_email', models.TextField()),
                ('hotel_password', models.TextField()),
                ('hotel_amenities', models.TextField()),
                ('hotel_phone_number', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Register_restaurant_user',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('restaurant_name', models.TextField()),
                ('restaurant_adress', models.TextField()),
                ('restaurant_cuisines', models.TextField()),
                ('restaurant_email', models.TextField()),
                ('restaurant_password', models.TextField()),
                ('restaurant_special_diets', models.TextField()),
                ('restaurant_phone_number', models.TextField()),
                ('restaurant_business_hours', models.TextField()),
            ],
        ),
    ]
