from django.apps import AppConfig


class TrippinappConfig(AppConfig):
    name = 'trippinapp'
