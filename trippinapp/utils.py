from amadeus import Client, ResponseError
from .models import *
import json

amadeus = Client(
        client_id = 'tFWhJdA95xsaqVxBjNU5wQjIocF8xClU',
        client_secret = 'tj01onHsJjeZMetH'
    )

def get_hotel(hotel_name, hotel_cityName):
    print("'" + str(hotel_cityName) + "'")
    try:
        response = amadeus.shopping.hotel_offers.get(
            cityCode = str(hotel_cityName),
            #name = str(hotel_name)
        )
        for x in response.data:
            #Pega o nome do hotel e joga na variavel, depois compara com o que o o usuario cadastrado e printa as amenities e o preço total
            nome_hotel = x.get("hotel").get("name")
            print(nome_hotel)
            if (nome_hotel == str(hotel_name)):
                #print(nome_hotel)
                amenities = x.get("hotel").get("amenities")
                #print(x.get("hotel").get("amenities"))
                #print("\n")
                price = x.get("offers")[0].get("price").get("total")
                phone = x.get("hotel").get("contact").get("phone")
                #print(x.get("offers")[0].get("price").get("total"))
                return str(amenities), str(price), str(phone)
            #else:
            #     print("Bruh")
    except ResponseError as error:
        print(error)

def get_restaurant(restaurant_name, latitude, longitude):
    try:
        response = amadeus.reference_data.locations.points_of_interest.get(
            latitude=latitude, 
            longitude=longitude)
        for x in response.data:
            name_restaurant = x.get("name")
            if (name_restaurant == str(restaurant_name)):
                print(name_restaurant)
                tags = x.get("tags")
                return tags
            else:
                 print("Bruh")
    except ResponseError as error:
        print(error)

def get_attraction(attraction_name, latitude, longitude):
    try:
        response = amadeus.shopping.activities.get(
            name=str(attraction_name),
            latitude=latitude, 
            longitude=longitude)
        for x in response.data:
            #Pega o nome do hotel e joga na variavel, depois compara com o que o o usuario cadastrado e printa as amenities e o preço total
            name_attraction = x.get("name")
            if (name_attraction == str(attraction_name)):
                print(name_attraction)
                price = x.get("price").get("amount")
                print(price)
                return str(price)
            else:
                 print("Bruh")
    except ResponseError as error:
        print(error)